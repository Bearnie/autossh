#!/usr/bin/env python3

#	Author:		Joseph Sadden
#	Date:		28th January, 2019
#	Revision:	1.0

#	Import Library Packages
import os, struct
import sys
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from getpass import getpass

#	Append the root folder for the project to the import path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(sys.argv[0]), "..")))

#	Import Local Files


#	Script-wide Definitions


#		SCRIPT PURPOSE
#
#		This script provides the ability to decrypt a file which was encrypted by this tool's compliment.
#		THIS IS NOT TO BE USED COMMONLY.  LEAVING DECRYPTED FILES AROUND IS A BAD IDEA.


#	main	-	Handles the process of decrypting the file, using the user-entered passphrase as the seed to generate the AES keys to decrypt the file.
#
#	Inputs:		fileToDecrypt	-	The encrypted file which should be decrypted by this.
#
#	Outputs:	decryptedFile	-	A new file, containing the result of decrypting using the provided key.  No gaurantees about the key being valid.
#
def main(fileToDecrypt):

	#	Prompt the user for the key to use to decrypt the file.
    key = getKey()

	#	Set the filename for the decrypted contents.
    decryptedFile = os.path.splitext(os.path.abspath(fileToDecrypt))[0] + ".decrypted"

	#	Set the chunk size to work with when decrypting.
    chunksize=24*1024

	#	Open the encrypred file in binary mode.
    with open(fileToDecrypt, 'rb') as infile:

		#	Read out how large the original file was.
        origsize = struct.unpack('<Q', infile.read(struct.calcsize('Q')))[0]

		#	Read out the initialization vector used in the encryption.
        iv = infile.read(16)
		
		#	Initialize the decryptor object
        decryptor = AES.new(key, AES.MODE_CBC, iv)

		#	Open the output file in binary mode.
        with open(decryptedFile, 'wb') as outfile:

			#	Until there is no more data...
            while True:

				#	Read out one chunk of data...
                chunk = infile.read(chunksize)
				
				#	If there's no more data, break out of the loop.
                if len(chunk) == 0:
                    break

				#	Otherwise, decrypt the data.
                outfile.write(decryptor.decrypt(chunk))

			#	Truncate the file down to the original size, removing all of the padding that was put in during encryption.
            outfile.truncate(origsize)

#	getKey	-	This function prompts the user to enter the passphrase they would like to use to encrypt the data.
#				This will immediately hash the value, and require the user enter the passphrase twice to reduce the chance of entering it wrong.
#
#	Inputs:		None
#
#	Outputs:	Hashed passphrase value.
#
def getKey():

	#	Prompt the user to enter the passphrase to use twice.
	k1, k2 = ("", "")
	k1 = SHA256.new(getpass("Enter the password to decrypt the file: ").encode()).digest()
	k2 = SHA256.new(getpass("Confirm the password to decrypt the file: ").encode()).digest()

	#	If the values don't match, prompt again, and keep doing so until two do match.
	while (k1 != k2):
		print("Passwords do not match!")
		k1 = SHA256.new(getpass("Enter the password to decrypt the file: ").encode()).digest()
		k2 = SHA256.new(getpass("Confirm the password to decrypt the file: ").encode()).digest()

	#	Return one of the matched pair of hashed values.
	return k1


#	printHelp	-	This function will print a help message if the user requests it.
#
#	Inputs:		None, this is called if the command line parameters "-h" or "--help" are passed in
#
#	Outputs:	None, prints the help menu to the terminal
#
def printHelp():

	print("""
	decryptSecretFile.py	-	A command-line tool for decrypting a file created with the encryptSecretFile.py tool.

	Usage:

	python3 decryptSecretFile.py [file] ... [--help]

	This tool takes in command-line arguments specifying the file or files to decrypt.
	These must be a white-space delimited list, but can include relative paths.
	Each file passed in will request the passphrase to use to decrpyt it.

	It will then prompt TWICE for a passphrase to use, ensuring the entered phrase matched in both entries.

	The decrypted version of the file will be placed into the same directory as the source file.

	""")

	return


#	Allow this script to be called from the command-line
if (__name__ == "__main__"):

	#	If arguments were passed in...
	if (len(sys.argv) > 1):

		#	Check if the help keywords were used, and print the help menu if so
		if ("--help" in sys.argv) or ("-h" in sys.argv):
			printHelp()

		#	Otherwise, send each other argument in to be encrypted, one at a time.
		else:
			for arg in sys.argv[1:]:
				main(arg)

	#	If no arguments were given, display the help menu
	else:
		printHelp()