#!/usr/bin/env python3

#	Author:		Joseph Sadden
#	Date:		3rd January, 2019
#	Revision:	1.0

#	Import Library Packages
import sys
import os

#	Import Local Files
from createNewScriptFile import main as newFile

#	Script-wide Definitions


#		SCRIPT PURPOSE
#
#		This script is designed to build a new empty python3 module, ready to be used to build a larger project.


#	main	-	The main entry point of this script.
#
#	Inputs:		None
#	Outputs:	None, the files and folder are made in the current directory
#
def main(folder="New Module"):

	#	Make the new directory for the module
	os.makedirs(folder)

	#	Create the main file
	newFile(os.path.join(os.getcwd(), folder, "main.py"), fileType="-m")

	#	Create the README file
	with open(os.path.join(os.getcwd(), folder, "README.txt"), "w+") as outFile:
		outFile.write("\n\tREADME.txt\n\n\t")

	#	Create the command-line help file
	with open(os.path.join(os.getcwd(), folder, "main_help.txt"), "w+") as outFile:
		outFile.write("")

	return


#	Allow this script to be called from the main function
if (__name__ == '__main__'):
	
	if (len(sys.argv) == 1):
		main()
	elif (len(sys.argv) == 2):
		main(sys.argv[1])