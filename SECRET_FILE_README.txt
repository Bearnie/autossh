
    Secret File README

    The AutoSSH "Secret" file is a file intended to contain the sudo passphrases for any remote hosts to connect to.
    As this would be a MAJOR security point, this file must remain encrypted, using the provided "encryptSecretFile.py" script.

    This script will hash a given passphrase, and use this as the seed to encrypt the file using a standard AES encryption scheme.
    Keep this passphrase secure, and do not forget it, as there is no way of recovering the data without this.

    The "decryptSecretFile.py" script is intended to be used in a VERY specific circumstance.  Should a "Secret" file need to be shared, this can be used
    to provide a plaintext version of the file, to prevent passphrase sharing.  The file should be re-encrypted by both parties as soon as possible after the sharing of the Secret file.

    The secret file is designed to be located as ~/.autoSSH/secret.crypt, and remain encrypted for proper use.
    The AutoSSH tool will decrypt the file temporarily during use, to be able to parse out the passphrase for a specific host.
    This process does NOT generate an un-encrypted version of the file at any point, and will be cleared at the end of the parsing function.

    To add an entry to the secret file, it must be in unencrypted format, and entries are expected for be formatted as follows:

    <host>[= ]<passphrase>

    Where <host> is the hostname as seen in the user's ~/.ssh/config file
    [= ] means there can be either an equals-sign or whitespace to separate the host from the passphrase
    <passphrase> is the exact passphrase to use.

    These entries are designed to be one-per-line of the file, and empty lines will be ignored.

    There is no ability for commented lines, as this file is intended to remain encrypted permanently.
