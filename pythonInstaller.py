#!/usr/bin/env python3

#	Author:		Joseph Sadden
#	Date:		1st Febraury, 2019
#	Revision:	1.0

#	Import Library Packages
good = True
try:
	import os
except ImportError:
	print("os library not installed...")
	good = False

try:
	import shutil
except ImportError:
	print("shutil library not installed...")
	good = False

try:
	import sys
except ImportError:
	print("sys library not installed...")
	good = False

try:
	import tarfile
except ImportError:
	print("tarfile library not installed...")
	good = False

try:
	import requests
except ImportError:
	print("requests library not installed...")
	good = False

try:
	import subprocess
except ImportError:
	print("subprocess library not installed...")
	good = False

if (not good):
	print("Please install the listed modules and restart the installer.")
	exit()

#	Script-wide Definitions
executableName = "autoSSH"
archiveName = f"{executableName}_Source.tar.gz"

installDirectoryBase = os.path.join(os.path.expanduser('~'), "tmp")
installDirectory = os.path.join(installDirectoryBase, executableName)
tarArchive = os.path.join(installDirectory, "master.tar.gz")
applicationDirectory = os.path.join(os.path.expanduser('~'), f'.{executableName}')
sourceBackup = os.path.join(applicationDirectory, "source")
versionFile = os.path.join(applicationDirectory, "version")

#	Get the set of directories which need to exist for the installer to function correctly.
directories = set([installDirectoryBase, installDirectory, applicationDirectory, sourceBackup])


#		SCRIPT PURPOSE
#
#		This script is designed to work as a cross-platform installer for the autoSSH tool.
#		It is written in python, to hopefully allow for an easier and more common implementation of the install process across different platforms.


#	main	-	This is the main entry point of the installer.  This will oversee the entire installation process.
#
#	Inputs:		None
#
#	Outputs:	None
#
def main():

	#	If the version file already exists, this implies the user has already installed the tool.
	#	Therefore don't bother checking for new versions of the python libraries.
	if (os.path.exists(versionFile) == False) or (True):
		checkLibraries()

	#	Download the latest copy of the source code from the repository
	#	If the current version is the newest version, remove the download and safely exit
	downloadSourceCode()

	#	Untar the source code, and check the version against the current version.
	if (unpackSource() == "Newest"):
		print("The latest version is already installed...")
		sys.exit(0)

	#	Since the downloaded version is not the newest, continue with the installation...
	#	Build the executable, using the included build script for the project.
	buildExecutable()

	#	Do a final clean of the installation folders, removing everything aside from the application folder
	cleanInstallation()

	print(f"Finished installation!\nThe autoSSH tool has been installed at: {applicationDirectory}/source")
	
	return


#	checkLibraries	-	This function will check to ensure that the required libraries are installed for the autoSSH tool.
#
#	Inputs:		None
#
#	Outputs:	None
#
def checkLibraries():

	good = True
	
	#	Try to import pip
	try:
		import pip
		print("pip found...")
	except ImportError:
		print("Pip not already installed.  Please install pip before continuing.")
		good = False

	#	Try to import fabric2
	try:
		import fabric2
		print("fabric2 found...")
	except ImportError:
		print("Fabric2 python library not installed.  Please install before continuing...\n\t'python3 -m pip install fabric2'")
		good = False
	
	#	Try to import PyInstaller
	try:
		import PyInstaller
		print("pyinstaller found...")
	except ImportError:
		print("PyInstaller library not installed.  Please install before continuing...\n\t'python3 -m pip install pyinstaller'")
		good = False

	#	Try to import PyCrypto
	try:
		import Crypto
		print("PyCrypto found...")
	except ImportError:
		print("PyCrypto library not installed.  Please install before continuing...\n\t'python3 -m pip install PyCrypto'")

	#	If any of the imports failed, exit out
	if (not good):
		print("Please install the specified package(s) and try to install again...")
		sys.exit(0)

	return


#	downloadSourceCode	-	This function will download the source code from the repository, into the globally defined "installDirectoryBase"
#
#	Inputs:		None
#
#	Outputs:	None, the file will be downloaded to the "installDirectoryBase" folder.
#
def downloadSourceCode():

	#	Notify the user of what's happening
	print("Downloading source code from repository...")

	#	Define the filename to save the source code tarball as.
	filename = f"{installDirectory}/master.tar.gz"
	url = "https://bitbucket.org/Bearnie/autossh/get/master.tar.gz"

	#	Try to download the code...
	try:
		r = requests.get(url)
		with open(filename, "wb") as inFile:
			inFile.write(r.content)
	
	#	Catch exceptions
	except Exception as e:
		print(f"Error occured while downloading repository!\n{e}.\nExiting...")
		sys.exit(0)

	#	Notify when this is finished...
	print(f"Downloaded source code to {installDirectory} ...")

	return


#	unpackSource	-	This function will unpack the repository tarball, checking the version number against the currently installed version (if any)
#
#	Inputs:		None
#
#	Outputs:	String identifying whether the installed version of the software matches the current version in the repository
#
def unpackSource():

	print("Unpacking downloaded source archive...")

	#	Unpack and decompress the tar.gz archive
	with tarfile.open(f"{tarArchive}", "r:gz") as archive:
		#	Extract everything from the archive...
		archive.extractall(path=installDirectory)

	#	Check versions
	unpackDirectory = [x for x in os.listdir(installDirectory) if "Bearnie" in x][0]
	downloadVersion = unpackDirectory.split('-')[-1]
	if (os.path.exists(versionFile)):
		installedVersion = open(versionFile, 'r').readline().strip()
	else:
		installedVersion = "Not installed"

	print(f"Downloaded: {downloadVersion} , Running: {installedVersion} .")

	#	If the most current version is already installed, remove the temporary installer files and exit safely
	if (downloadVersion == installedVersion):
		shutil.rmtree(installDirectory)
		return "Newest"

	#	Create and write the version file
	with open(versionFile, "w+") as outFile:
		outFile.write(downloadVersion)

	#	Move the tar archive into the source directory
	os.rename(tarArchive, os.path.join(sourceBackup, archiveName))

	#	Move all of the files from the unpacked tar archive into the current directory
	for f in os.listdir(os.path.join(installDirectory, unpackDirectory)):
		try:
			os.rename(os.path.join(installDirectory, unpackDirectory, f), os.path.join(installDirectory, os.path.basename(f)))
		except:
			print(f"Could not move file {f}.")

	#	Remove the archive directory
	shutil.rmtree(os.path.join(installDirectory, unpackDirectory))

	print("Unpacked latest version...")

	return ""


#	buildExecutable	-	Using the PyInstaller tool, build the source code into a single executable file.
#
#	Inputs:		None
#
#	Outputs:	None, the executable is built and located in it's own directory.
#
def buildExecutable():

	print("Building executable from Python source code...")
	oldDir = os.getcwd()
	os.chdir(installDirectory)

	#	PyInstaller must be called from the command-line
	subprocess.call(['pyinstaller', '--onefile', os.path.join("AutoSSHRunner", "autoSSH.py")])
	os.chdir(oldDir)

	print("Moving installation files into the right location...")

	#	Move the executable from the dist folder
	for f in os.listdir(os.path.join(installDirectory, "dist")):
		try:
			os.rename(os.path.join(installDirectory, "dist", os.path.basename(f)), os.path.join(sourceBackup, f))
		except:
			print(f"Could not move file: {f}")

	#	Move the README and python helper files into the source backup directory...
	for f in os.listdir(installDirectory):
		if (f.endswith(".txt")):
			try:
				os.rename(os.path.join(installDirectory, f), os.path.join(sourceBackup, os.path.basename(f)))
			except:
				print(f"Could not move file: {f}")
		elif (f.endswith(".py")):
			if not (f.startswith("create")):
				try:
					os.rename(os.path.join(installDirectory, f), os.path.join(applicationDirectory, os.path.basename(f)))
				except:
					print(f"Could not move file: {f}")

	print(f"Installing update tool to {applicationDirectory} ...")

	#	Rename the install script to reflect that it should now be used as the updater.
	os.rename(os.path.join(applicationDirectory, "pythonInstaller.py"), os.path.join(applicationDirectory, "updateAutoSSH.py"))
	
	return


#	cleanInstallation	-	This function will remove all of the temporary build and installation folders.
#
#	Inputs:		None
#
#	Outputs:	None
#
def cleanInstallation():

	print("Removing temporary installation and build folders...")

	#	Remove all of the installation folders, aside from the output application folder
	#	Remove the temporary build directories
	shutil.rmtree(installDirectoryBase)

	return

#	Allow this script to be called from the command-line
if (__name__ == "__main__"):

	#	Try to remove the old source backup folder...
	if (os.path.exists(sourceBackup) == True):
		try:
			shutil.rmtree(sourceBackup)
		except:
			print("Could not replace existing source backup folder...")

	#	Ensure all of the necessary directories for the installer exist.
	for d in directories:
		if (os.path.exists(d) == False):
			os.makedirs(d, True)

	main()