
    Host Files:

    This tool makes use of a "hosts" file, to allow batching of SSH/SFTP tasks across a large number of remote hosts.
    The formatting of this file is very basic, and can be editted on a per-job basis.

    Within the file will be a list of hosts to connect to, possible comments, and whitespace lines for readability.

    Blank lines are completely ignored, and can be inserted as-desired to increase readability of the file.

    Anything following a "#" will be treated as a comment.
    This allows one to give more descriptive names or descriptions to a host of set of hosts within the file.
    This also allows one to quickly remote or re-insert hosts in a file without needing much editting.

    To specify an actual host to connect to when the application is run, the hosts file will reference the user's "~/.ssh/config" file for
        the exact SSH parameters to use.  As such, the hostname within the AutoSSH hosts file must match the host alias within the ssh config file.

    For example, if there is an entry for host "alice" within my ssh config file:

        Host alice
          Hostname 192.168.0.10
          User Alice
          Port 22

    Then I would add either of the following entries to the AutoSSH hosts file:

        Host=alice
        Host alice

    Note, the identifier is either "Host=" or "Host ".  Any further whitespace will be trimmed off, and any comments AFTER the identifier will be stripped off during runtime.