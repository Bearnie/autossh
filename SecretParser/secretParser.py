#!/usr/bin/env python3

#	Author:		Joseph Sadden
#	Date:		28th January, 2019
#	Revision:	1.0

#	Import Library Packages
import os
import sys
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from getpass import getpass
import random, struct

#	Append the root folder for the project to the import path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(sys.argv[0]), "..")))

#	Import Local Files


#	Script-wide Definitions


#		SCRIPT PURPOSE
#
#		This script provides the means to decrypt the user's AutoSSH secret file, to return the sudo password for the given host.
#		Everything is based on being encrypted/decrypted with a user-entered passphrase for the secret file, and will remain encrypted.


#	main	-	Handles the process of decrypting the secret file, and parsing for the sudo pass for the current host.
#
#	Inputs:		secretFile	-	The path to the ENCRYPTED secret file.
#				secretPass	-	The hashed version of the passphrase for the secret file.
#				host		-	The current host to search for.
#
#	Outputs:	passphrase	-	The sudo passphrase for the given host.
#
def main(secretFile, secretPass, host):

	#	Initialize the chunksize for the decryption.
	chunksize=64*1024

	#	Initialize the passphrase to something known to be incorrect, to default to not allowing sudo access.
	passphrase = ""

	#	Open the "secret" file
	with open(secretFile, 'rb') as infile:
		#	Unpack the file, returning the size of the original file
		_ = struct.unpack('<Q', infile.read(struct.calcsize('Q')))[0]

		#	Read the initialization vector used during the encryption.
		iv = infile.read(16)
		
		#	Initialize an AES decryptor.
		decryptor = AES.new(secretPass, AES.MODE_CBC, iv)

		#	Initialize an empty string to hold the contents of the file...
		decryptedFile = ""

		#	Read the file, decrypting in chunks and appending to the string
		while True:
			chunk = infile.read(chunksize)
			if len(chunk) == 0:
				break
			decryptedFile += decryptor.decrypt(chunk).decode()

		#	Truncate down to the original size of the file.
		decryptedFile.strip(' ')

	#	Split on newlines, to parse out the actual passphrase details.
	decryptedFile.split('\n')

	#	Assert that the file is in newline-separated list format.
	if decryptedFile is not list:
		decryptedFile = [decryptedFile]

	#	For each line of the file...
	for line in decryptedFile:

		#	If the line starts with the given host...
		if line.startswith(host):

			#	Replace any '=' with a space, and return whatever is after the first space
			line = line.replace('=', ' ')
			passphrase = line.split(' ')[1].strip()
			decryptedFile = ""
			return passphrase

	return ""
	

#	getKey	-	This function prompts the user to enter the passphrase they would like to use to encrypt the data.
#				This will immediately hash the value, and require the user enter the passphrase twice to reduce the chance of entering it wrong.
#
#	Inputs:		None
#
#	Outputs:	Hashed passphrase value.
#
def getKey():

	secretPass, secretPass2 = ("", "")
	secretPass = SHA256.new(getpass("Enter the password to decrypt your \"Secret\" file: ").encode()).digest()
	secretPass2 = SHA256.new(getpass("Confirm password to decrypt \"Secret\" file: ").encode()).digest()
	
	while (secretPass != secretPass2):
		print("Password does not match.\n")
		secretPass = SHA256.new(getpass("Enter the password to decrypt your \"Secret\" file: ").encode()).digest()
		secretPass2 = SHA256.new(getpass("Confirm password to decrypt \"Secret\" file: ").encode()).digest()

	return secretPass
