
	README.txt

	This file contains a description of the SecretParser module.
	This module is designed to decrypt the AutoSSH "Secret" file, containing the sudo passphrases for the remote hosts defined in the "hosts" file.
	This secret file is only used if the "sudoFlag" is set, indicating that commands and scripts should be run using sudo on the remote end.

	This Secret file is encrypted using standard AES encryption, with a hashed user-specified passphrase as the key.

	This module contains two functions, described below:

		main	-	The main function which actually decrypts the Secret file, and parses for the host-specific details.
		getKey	-	Prompts the user for the decryption key, requiring two matching entries.

	