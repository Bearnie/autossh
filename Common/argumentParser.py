#!/usr/bin/env python3

#	Author:		Joseph Sadden
#	Date:       15th January, 2019
#	Revision:	1.0

#		SCRIPT PURPOSE
#
#		This script is designed to handle argument parsing and formatting for command-line arguments


#	formatArguments	-	This function takes in the arguments, as well as a dictionary of the arguments, and formats them into a list of tuples, with (identifier, value) format
#
#	Inputs:		validArguments		-	List of valid arguments for the module at hand
#				receivedArguments	-	The actual arguments as passed on the command-line
#				defaultValues		-	List of default values to use if no command-line value is provided
#
#	Outputs:	The argument values as a list, sorted into alphabetical order based on the argument key, containing the updated values from the arguments and default values otherwise
#
def formatArguments(validArguments, receivedArguments, defaultValues):

    #   Create the argument dictionary, using the valid arguments as the key
    formattedArguments = {validArguments[i]: defaultValues[i] for i in range(len(validArguments))}
    
	#	If the help argument is specified, display the help menu
    if ("--help" in receivedArguments):
        displayHelpMenu()
        return None
    
    #   Search through all of the recieved arguments
    for index, arg in enumerate(receivedArguments):
        #   If it's a valid argument...
        if (arg in validArguments):
            try:
                formattedArguments[arg].append(receivedArguments[index+1])
            except:
                print(f"Invalid argument specified for {arg}.")
                displayHelpMenu()
                return None

    formattedArguments = fixNonListArguments(formattedArguments)

    return formattedArguments.values()


#   fixNonListArgument  -   This function will assert that the arguments which are not expected to be in list form are not in list form.
#
#   Inputs:     formattedArguments  -   The dictionary containing a list of values for each argument keyword.
#
#   Outputs:    formattedArguments  -   The same dictionary, but with the arguments not expected to be lists reverted to singletons.
#
def fixNonListArguments(formattedArguments):

    #   Fix the config file argument, using the default value, or the given value if present
    if (len(formattedArguments["-c"]) == 1):
        formattedArguments["-c"] = str(formattedArguments["-c"][0])
    else:
        formattedArguments["-c"] = str(formattedArguments["-c"][1])

    #   Fix the hosts file argument, using the default value, or the given value if present
    if (len(formattedArguments["-h"]) == 1):
        formattedArguments["-h"] = str(formattedArguments["-h"][0])
    else:
        formattedArguments["-h"] = str(formattedArguments["-h"][1])

    #   Fix the remote directory argument, using the default value, or the given value if present
    if (len(formattedArguments["--remotedir"]) == 1):
        formattedArguments["--remotedir"] = str(formattedArguments["--remotedir"][0])
    else:
        formattedArguments["--remotedir"] = str(formattedArguments["--remotedir"][1])

    #   Fix the sudo Flag argument, using the default value, or the given value if present
    if (len(formattedArguments["--sudoFlag"]) == 1):
        formattedArguments["--sudoFlag"] = False
    else:
        formattedArguments["--sudoFlag"] = True

    return formattedArguments


#	displayHelpMenu	-	This function displays a given help file should the user pass an invalid argument, or the --help argument
#
#	Inputs:		None
#
#	Outputs:	None
#
def displayHelpMenu():

	print("""
    autoSSH.py    -   A python3 tool to allow automation and batching of SSH connections, running scripts remotely, and transferring files between remote hosts.

    This file can be called from the command-line:

    python3 autoSSH.py <-c configFile> <-h hostsFile> <-b before> <-a after> <--send fileList1> <--get fileList2> <--run fileList3> <--remotedir directory> <--help>

    -c  configFile      -   (optional) Configuration file to use.  NOT IMPLEMENTED YET.
                            This file contains configuration information for how to connect to non-ssh clients.

    -h  hostsFile       -   (optional) Specifies the remote hosts to connect to.
                            The format of this can be found in the main project README.txt or man pages.

    -b  before          -   (optional) List of shell commands to execute BEFORE any of the (possible) executable files are run.
                            Each command should be formatted as a single-quoted string, and must be so if there are any whitespace characters in the command.

    -a  after           -   (optional) List of shell commands to execute AFTER any of the (possible) executable files are run
                            but before attempting to retrieve the (possibly) specified files.
                            Each command should be formatted as a single-quoted string, and must be so if there are any whitespace characters in the command.

    --send  file        -   (optional) Path to a file on the local host to send but not run on the remote host(s).
                            Should be a single-quoted string.

    --get   file        -   (optional) Path to a file on the remote host(s) to SFTP back to the local host.
                            Should be a single-quoted string.

    --run   file        -   (optional) Path to a file on the local host to send and run on the remote host(s).
                            Should be a single-quoted string.

    --remotedir directory   -   (optional) The remote directory to work out of on the remote hosts.
                                By default, this will be "/tmp/AutoSSH_TRANSFER/" on the remote host.
                                Must be a single quoted string, with absolute paths.
                                Recommended to use the "/tmp" partition of the remote host.

    --sudoFlag <True/False> -   (optional) This argument determines whether or not the operations should run under "sudo".
                                This will apply to ALL operations on the remote host.
                                This will not work unless the remote host allows sudo without tty.

    --help              -   (optional) Displays this help menu and then exits

    Multiple values are allowed for the "-b", "-a", "--send", "--get", and "--run" arguments.
    All other arguments will only use the first value passed, or the default if none are given.

    When entering multiple values for the allowed arguments, the values can simple be added as additional parameter pairs.

    For example, if there are TWO commands which are desired to be run before any scripts are executed, that would be accomplished as follows:

        -b "echo Hello!" -b "echo Starting scripts soon..."

    Do note, that environment variables are evaluated using YOUR values BEFORE the commands are send to the server.
    If you want to use environment variables of the remote machine, you will need to encapsulate the commands in a script file.

    For multiple files, the process is the same.  Do note that the "--get" command does require an absolute path to each file on the remote machine.

    The order in which this operates is as follows:

        1)  Create the temporary working directory, if any files were specified by --send or --run
        2)  Transfer all files specified by --send and --run
        3)  Execute all commands specified by -b
        4)  Execute all files specified by --run
        5)  Execute all commands specified by -a
        6)  Retrieve all files specified by  --get, downloading them to your ~/Downloads/AutoSSH/ folder.
        7)  Clean up the temporary working directory, removing all files specified by --run

    By default, there exists a "config" and "hosts" file in the working directory of the application ("~/.autoSSH/"), and these are the default values for these arguments.

    All output from the commands or scripts being run will be printed to the local terminal.
    Any errors will be appended to a daily log file, located in ~/.autoSSH/logs/ """)

	return