
        README.txt

        This file contains a description and explanation for each of the Python files within the "Common" Module.
        These files are all written for python3, specifically the Cython 3.6 interpreter.
        This module is intended to contain small, general functions and tools, which are used by multiple other modules within the greater PBID Process project.
        Each script contains functions related to a single small task.

    This module contains the following 2 files:

        argumentParser.py
        logFile.py

    Each of these files will be further explained below:

==================================================
    argumentParser.py

    This file is intended to provide a common interface for parsing command-line parameters.
    This file contains 2 functions, and only "formatArguments" is intended to be called from outside.
    
        "formatArguments" will read in the command-line arguments as a space-separated list, compare them to a dictionary of valid arguments, and either update the parameters of the module, or provide the default arguments

        "displayHelpMenu" will read the contents of a module-specific help file (defined in the module, and located in the same directory) to indicate the command-line parameter calling convention and arguments for the module

    The "formatArguments" is intended to be called from the "if (__name__=='__main__'):" block of a script, and below is an example usage:

        if (__name__ == '__main__'):
    
        #   Check the arguments to see if any of the default parameters need to be overwritten
        if (len(sys.argv) > 1):
            formattedArguments = formatArguments(validArguments, sys.argv[1:], defaultValues, helpFile)
            main(*formattedArguments)
        #   Call the main function with the default values
        else:
            main(*defaultValues)

    The validArguments should be a list, containing the argument keys (i.e. the -<?> or --<?> options)
    The defaultValues should be a list, providing the default arguments to use, in the same order as the argument keys within validArguments
    The ordering is important, as the values passed to the main function will be unpacked from the list of returned values, so this needs to match the positional ordering of the arguments


==================================================
    logFile.py:

    This file is intended to provide a common interface for many different functions, modules, and classes to generate and append to log files.
    Logging status and progress is an incredibly valuable resource, and this is doubly important for this PBID project, as the process is intended to run as a background process.
    This file contains default values, to allow simple incorporation with other functions.  By default, the log file will be generated in the current working directory, and named <datetime.now()>.log
    This file contains 5 functions, although only the "appendToLog" function is intended to be utilized as the interface with other code.

        "logExists" will take in the a given filepath, and check if that exists as a log already.  If specific path is provided, it will utilize the default arguments and check for that file.

        "createLog" will create a new log file, either with a supplied filepath and filename, or the default values if none is given.

        "deleteLog" will delete a specified log file.  If none is supplied, it will delete the default log file.

        "makeFullPath" will generate a valid log filepath and filename.  If the path provided does not end with ".log", it will append the default log filename to the given path.

        "appendToLog" is intended to be the function imported by other code.  This will generate a new log file if necessary, and will append a given line to the log.
            This requires one of the status codes, a message body, and the name of the calling function.  The status code must be one of the pre-defined values in the header of this file.
            To ignore the other arguments (if desired), empty strings ("") can be passed as the arguments.