#!/usr/bin/env python3

#   Author:     Joseph Sadden
#   Company:    Tantalus Systems
#   Date:       5th December, 2018
#   Revision:   1.0

#   Import Library Packages
import os
from datetime import date, datetime

#   Script-wide Definitions
logFileBaseDirectory = os.getcwd()
logFileName = f"{date.today()}.log"
logStatusCode = "STATUS"
logWarningCode = "WARNING"
logErrorCode = "ERROR"
logFileCodes = [logStatusCode, logWarningCode, logErrorCode]


#       SCRIPT PURPOSE
#
#       This script is intended to provide a single interface for generating and appending to error/warning/status log files in the standard operation of the parent script.  It intends to provide as common an interface as is feasable, to allow these functions to be utilized across the entire PBID Server/Process.  


#   CODE BELOW

#   logExists   -   This function takes in a path, and returns a Boolean flag whether or not a log file exists on the path
#
#   Inputs:     Path    -   String containing the path to be tested, this path is from the base pbid log directory defined above
#
#   Outputs:    Boolean flag indicating whether a log file exists on the path
#
def logExists(path):

    #  Build the full path to be tested
    fullPath = makeFullPath(path)

    #   Check if there exists a log file with the given path, and return the status as a boolean
    return os.path.isfile(fullPath)


#   createLog   -   This function takes in a path, creates a new empty log file at the given path, relative to the base log directory, returning the full log path as a string
#
#   Inputs:     path        -   String containing the path to the directory to place the log file
#
#   Outputs:    fullPath    -   String containing the full path to the log file, from "/"
#
def createLog(path):

    #  Build the full path to be tested, prepending the base log path if needed, and appending the filename as well
    fullPath = makeFullPath(path)

    #   Try to create the log file
    if (logExists(fullPath) == False):
        try:
            #   Create a new file in write mode if it doesn't already
            os.makedirs(os.path.dirname(fullPath), exist_ok=True)
            f= open(fullPath, "w+")
            f.write(f"Log File - started: {datetime.now().replace(microsecond=0).isoformat(' ')}\n\n")
            f.close()

        #   Catch any exceptions, and append to the log file
        except:
            print("Exception occured when creating log file!")
    else:
        print("Log already exists, not overwriting.")

    return fullPath


#   deleteLog   -   This function safely deletes the a given log file
#
#   Inputs:     path    -   String containing the full path, filename inclusive, to the log file to delete
#
#   Outputs:    Int - Success code, 0 = success, -1 = fail
#
def deleteLog(path):

    #   Make sure the log file actually exists
    if (logExists(path)):
        #   Try to remove the log file
        try:
            os.remove(path)
            return 0
        except:
            print("Exception occurred when deleting given log file: " + path)

    return -1


#   makeFullPath    -   This function takes a given path, and asserts that it will be a relative path from the pbid log directory, and that it includes a unique filename for the log file
#
#   Inputs:     testPath    -   A string containing the path to test for correctness
#
#   Outputs:    fullPath    -   A string containing the correct path to a unique log-file
#
def makeFullPath(testPath=logFileBaseDirectory):

    #   Prepare the return value.  If it ends up being correct, this function will do nothing
    fullPath = testPath

    #   Check if the given path ends with a *.log filename
    if (testPath.endswith(".log") == False):
        fullPath = os.path.join(fullPath, logFileName)

    return fullPath


#   appendToLog -   This function performs the act of appending a given message to the given log file
#
#   Inputs:     path        -   A string containing the path to the log file to append to
#               status      -   A string containing the type of message to append to the log (Status, Warning, Error)
#               message     -   A string containing the actual message to append to the log
#               callerName  -   A string containing the name of the function which calls appendToLog
#
#   Outputs:    int, determines the state of writing to the log, -1 = error, 0 = success
#
def appendToLog(path, status, message, callerName):

    #   If no path is given, set the path as the default log file location and create a log there
    if (path is None):
        path = makeFullPath()
        if (logExists(path) == False):
            createLog(path)

    path = makeFullPath(testPath=path)

    #   If the status code given is not one of the valid codes, this is an error to append to the log
    if (status not in logFileCodes):
        raise Exception("No status code given!")

    #   If the message given is blank, this is an error
    if (message is None):
        raise Exception("No log message given!")

    #   If no calling function is given, this is an error
    if (callerName is None):
        raise Exception("No calling function given!")

    #   Try to open the log file, and append the given message to it
    try:
        with open(path, "a+") as log:
            log.write(f"{status}: {message}. {callerName}. {datetime.now().replace(microsecond=0).isoformat(' ')}\n")
            return 0

    #   Catch any exceptions, and add them to the log
    except Exception as e:
        print("Exception occured when appending to log file! " + str(e) + ".")

    return -1


