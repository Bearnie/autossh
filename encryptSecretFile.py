#!/usr/bin/env python3

#	Author:		Joseph Sadden
#	Date:		28th January, 2019
#	Revision:	1.0

#	Import Library Packages
import os
import sys
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from getpass import getpass
import random, struct

#	Append the root folder for the project to the import path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(sys.argv[0]), "..")))

#	Import Local Files


#	Script-wide Definitions


#		SCRIPT PURPOSE
#
#		This script provides the ability to encrypt the file intended to contain the sudo passwords for the remote hosts to connect to with the AutoSSH tool.
#		This file will be encrypted using standard AES encryption, using a salted hash of a user-entered password as the seed to generate the key.


#	main	-	This function handles the actual encryption of the file, prompting the user for the password to use as the seed.
#
#	Inputs:		fileToEncrypt	-	The path to the file to encrypt.  This file will be deleted following the running of this script.
#
#	Outputs:	The file is encrypted and saved to the current working directory, and the original file is deleted.
#
def main(fileToEncrypt):

	#	Prompt the user for the passphrase to use as the seed.
	key = getKey()

	#	Define the output file name.
	encryptedFile = os.path.splitext(os.path.abspath(fileToEncrypt))[0] + ".crypt"

	#	Set the chunk size to use for the encryption.
	chunksize=64*1024

	#	Generate a 16-byte random value to use as the initialization vector of the encryptor.
	iv = os.urandom(16)
	
	#	Initialize the AES object to do the encryption.
	encryptor = AES.new(key, AES.MODE_CBC, iv)

	#	Save the original file length.
	filesize = os.path.getsize(fileToEncrypt)

	#	Open the input file for reading in binary mode.
	with open(fileToEncrypt, 'rb') as infile:

		#	Open the output file for writing in binary mode.
		with open(encryptedFile, 'wb') as outfile:

			#	Write the filesize, followed by the initialization vector.
			outfile.write(struct.pack('<Q', filesize))
			outfile.write(iv)

			#	Until the file runs out of bytes...
			while True:
				#	Read a chunk of data...
				chunk = infile.read(chunksize)

				#	If this is the end, exit.
				if len(chunk) == 0:
					break
				#	If the chuck doesn't lie on a proper boundary, pad with spaces.
				elif len(chunk) % 16 != 0:
					chunk += bytes((((16 - len(chunk)) % 16) * ' ').encode())

				#	Encrypt the chunk and write it to the output file.
				outfile.write(encryptor.encrypt(chunk))

	return


#	getKey	-	This function prompts the user to enter the passphrase they would like to use to encrypt the data.
#				This will immediately hash the value, and require the user enter the passphrase twice to reduce the chance of entering it wrong.
#
#	Inputs:		None
#
#	Outputs:	Hashed passphrase value.
#
def getKey():

	#	Prompt the user to enter the passphrase to use twice.
	k1, k2 = ("", "")
	k1 = SHA256.new(getpass("Enter the password to encrypt the file: ").encode()).digest()
	k2 = SHA256.new(getpass("Confirm the password to encrypt the file: ").encode()).digest()

	#	If the values don't match, prompt again, and keep doing so until two do match.
	while (k1 != k2):
		print("Passwords do not match!")
		k1 = SHA256.new(getpass("Enter the password to encrypt the file: ").encode()).digest()
		k2 = SHA256.new(getpass("Confirm the password to encrypt the file: ").encode()).digest()

	#	Return one of the matched pair of hashed values.
	return k1


#	printHelp	-	This function will print a help message if the user requests it.
#
#	Inputs:		None, this is called if the command line parameters "-h" or "--help" are passed in
#
#	Outputs:	None, prints the help menu to the terminal
#
def printHelp():

	print("""
	encryptSecretFile.py	-	A command-line tool for encrypting a file based on a user-specified passphrase.

	Usage:

	python3 encryptSecretFile.py [file] ... [--help]

	This tool takes in command-line arguments specifying the file or files to encrypt.
	These must be a white-space delimited list, but can include relative paths.
	Each file passed in will request the passphrase to use to encrpyt it.

	It will then prompt TWICE for a passphrase to use, ensuring the entered phrase matched in both entries.

	The encrypted version of the file will be placed into the same directory as the source file.

	""")

	return


#	Allow this script to be called from the command-line
if (__name__ == "__main__"):

	#	If arguments were passed in...
	if (len(sys.argv) > 1):

		#	Check if the help keywords were used, and print the help menu if so
		if ("--help" in sys.argv) or ("-h" in sys.argv):
			printHelp()

		#	Otherwise, send each other argument in to be encrypted, one at a time.
		else:
			for arg in sys.argv[1:]:
				main(arg)

	#	If no arguments were given, display the help menu
	else:
		printHelp()