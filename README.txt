
    README.txt

    autoSSH     -   A compiled python3 tool for automating and batching SSH commands and scripts for multiple remote hosts.

    This tool is intended to be called from the command-line.

    <path to executable>/./autoSSH

    This tool has the following command-line parameters, which can be specified in any order.

    -c      configFile      -   This is not yet implemented.

    -h      hostsFile       -   A file containing the list of remote hosts to connect to for the current invocation.

    -b      preCommands     -   A list of shell commands to execute (in the order they are specified) once the SSH connection to the remote host
                                has been established, and before any files specified with the --send or --run arguments have been transferred.

    -a      postcommands    -   A list of shell commands to execute (in the order they are specified) after all of the files designated --run have completed,
                                but before the files specified by --get have been retrieved.

    --send  filesToSend     -   A list of files to transfer to the remote host, into the directory specified by --remotedir.  These will not be executed, and will
                                simply be transferred to the remote host over an SFTP connection.

    --run   filesToRun      -   A list of files to transfer to the remote host, into the directory specified by --remotedir.  These files will be executed on the remote host,
                                but will not necessarily have access to environment variables.

    --get   filesToGet      -   A list of files on the remote host (using absolute paths on the remote host) to SFTP back to the local machine.  These will be retrieved AFTER
                                all of the commands and scripts have been run, and will be downloaded to the user's Downloads/AutoSSH/ folder.

    --remotedir directory   -   The path to the directory on the remote server to upload any files specified by --send or --run.  This will be deleted at the end of the connection
                                to the remote host, unless the --send argument contains files.

    --sudoFlag <anything>   -   A flag indicating whether or not the commands and script should be run as sudo.  This will apply to ALL commands and scripts for the current
                                invocation.  By default this is set to False, but any value passed in will set this to True.
                                If this is set to True, the ~/.autoSSH/secret.crypt file must be present and contain the sudo passphrases for the remote hosts, or else this will encounter errors.

    --help                  -   Displays a help menu, and exits


    multiple values can be specified for the "-b", "-a", "--send", "--run", "--get" arguments.
    One value should be specified for each flag.

        e.g.        autoSSH -b "echo Hello World!" -b "hostname"

        is the expected method of passing two commands to be executed BEFORE any scripts.

    For more information on the Secret or Hosts files, please see the specific README files.

    This tool can also be incorporated into larger Python scripts, as the source code is included within the source/ subdirectory of where it is installed.