#!/usr/bin/env python3

#	Author:		Joseph Sadden
#	Date:		28th January, 2019
#	Revision:	2.0

#	Import Library Packages
import os
import sys
import re
from fabric2 import Connection, Config
from datetime import date
from time import sleep

#	Filter warnings (the cryptography libraries throw a bunch of deprecated issues)
# import warnings
# warnings.filterwarnings("ignore")

#   Append the root folder for the project to the import path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(sys.argv[0]), "..")))

#	Import Local Files
from Common.logFile import appendToLog as log, logErrorCode, logWarningCode, logStatusCode, logExists, createLog, makeFullPath	#	Import the local logging module.
from Common.argumentParser import formatArguments   #   Import the command-line checking functionality for this module.
from Operations import main as execute				#	Import the top-level operations function, to handle the overall AutoSSH process.
from SecretParser.secretParser import main as parseSecretFile, getKey


#	Script-wide Definitions
applicationFolder = os.path.expanduser("~/.autoSSH")
logFilePath = makeFullPath(f"{applicationFolder}/logs")
localDownloadFolder = os.path.expanduser(f"~/Downloads/AutoSSH/{date.today()}/")
passwordFile = os.path.expanduser("~/.autoSSH/secret.crypt")

ConnectionTimeoutValue = 60	#	Seconds to wait for timeout on SSH connection.

#	Default arguments	-	DO NOT CHANGE
defaultConfigFile = [f"{applicationFolder}/config",]
defaultHostFile = [f"{applicationFolder}/hosts",]
defaultBeforeCommands = []
defaultAfterCommands = []
defaultFilesToSend = []
defaultFilesToRetrieve = []
defaultFilesToExecute = []
defaultRemoteFolder = ["/tmp/AutoSSH_TRANSFER",]
defaultSudoFlag = [False,]

#	Command-line Argument Handling
validArguments = ["-c", "-h", "-b", "-a", "--send", "--get", "--run", "--remotedir", "--sudoFlag"]
defaultArguments = [defaultConfigFile, defaultHostFile, defaultBeforeCommands, defaultAfterCommands, defaultFilesToSend, defaultFilesToRetrieve, defaultFilesToExecute, defaultRemoteFolder, defaultSudoFlag]


#		SCRIPT PURPOSE
#
#		This script is designed to run arbitrary command-line operations on a variable number of remote hosts.
#		The goal of this tool is to automate ssh-level access and command-line manipulation for an arbitrary number of remote hosts.


#	main	-	This function will handle the top-level process of connecting to various hosts, running the desired command(s), and catching the output.
#
#	Inputs:		configFile		-	The path to the application config file to use for this instance.
#				hostFile		-	The path to the file containing the list of hosts to connect to.  These hosts are intended to match the aliased name found in the user's ~/.ssh/config file.
#				preCommands		-	A list of shell commands to run BEFORE all of the scripts are run on the remote host.
#				postComands		-	A list of shell commands to run AFTER all of the scripts are run on the remote host, but before any of the requested files are returned to the local host.
#				filesToSend		-	A list of files to send, but not run, on the remote host.
#				filesToRetrieve	-	A list of files to retrieve from the remote host.
#				filesToExecute	-	A list of files to send and then run on the remote host.
#				remoteFolder	-	The folder path to upload all of the files to on the remote host.
#				sudoFlag		-	Indicated whether or not all of the commands and script should be run with the "sudo" flag.
#
#	Outputs:	The output from the commands/scripts, will be printed to the local terminal
#
def main(	configFile=defaultConfigFile[0],
			hostFile=defaultHostFile[0],
			preCommands=defaultBeforeCommands,
			postCommands=defaultAfterCommands,
			filesToSend=defaultFilesToSend,
			filesToRetrieve=defaultFilesToRetrieve,
			filesToExecute=defaultFilesToExecute,
			remoteFolder=defaultRemoteFolder[0],
			sudoFlag=defaultSudoFlag[0]):

	#	If nothing has been specified to be done, don't do anything
	if (preCommands == []) and (postCommands == []) and (filesToSend == []) and (filesToRetrieve == []) and (filesToExecute == []):
		print("No actions given to execute on remote hosts.\nExiting...")
		return

	#	Ensure that the application directory exists for this application, and create the default template files within.
	createAppliationFolder(logFilePath)
		
	#	Read the ~/.autoSSH/hosts file, to determine which hosts to connect to (symbolic name, as shown in ~/.ssh/config).
	hosts = list(dict.fromkeys(readHostFile(hostFile, logFilePath)))

	if (sudoFlag):
		#	Get the passphrase for the secret file.
		secretPass = getKey()

	#	For each host...
	for host in hosts:

		#	Notify the user of the current host.
		print(f"Connecting to: {host}...")

		#	Parse out the sudo password for this host (if required).
		if (sudoFlag):
			passphrase = parseSecretFile(passwordFile, secretPass, host)
			#	Override the default sudo responder value
			config = Config(overrides={'sudo': {'password': passphrase}})
		else:
			config=Config()

		#	Open a connection to the remote host...
		with Connection(host, config=config, connect_timeout=ConnectionTimeoutValue) as c:
			
			#	Do all of the necessary behaviour for the AutoSSH process.
			try:
				execute(c, host, remoteFolder, filesToSend, preCommands, filesToExecute, postCommands, filesToRetrieve, localDownloadFolder, sudoFlag, logFilePath)

			#	Catch exceptions and log them...
			except Exception as e:
				log(logFilePath, logErrorCode, f"Error occured while running on {host}.  {e}", "AutoSSH-main")
				print(f"Exception occurred with host - {host} : {e}\n")
				passphrase = ""
				c.close()
			
			#	Catch keyboard interrupt and safely close.
			except KeyboardInterrupt:
				print("\nExiting...\n")
				passphrase = ""
				c.close()
				return

		#	Add some space between hosts...
		print("")
		sleep(1)

	return


#	createApplicationFolder	-	This function ensures that this application has a proper application folder in the user's home folder, and the expected files are present
#
#	Inputs:		logFilePath	-	The path to the log file to use for this application
#
#	Outputs:	None
#
def createAppliationFolder(logFilePath):

	#	Try to create the directories and folders
	try:
		#	Create the base application folder if it doesn't exist
		if (os.path.exists(applicationFolder) == False):
			os.makedirs(applicationFolder)

		#	Create the log file if it doesn't exist
		if (logExists(logFilePath) == False):
			createLog(logFilePath)

		#	Create the config file if it doesn't exist
		configFile = os.path.join(applicationFolder, "config")
		if (os.path.exists(configFile) == False):
			with open(configFile, "w+") as outFile:
				outFile.write("#\tThis file is not used yet\n\n")

		#	Create the hosts file if it doesn't exist
		hostsFile = os.path.join(applicationFolder, "hosts")
		if (os.path.exists(hostsFile) == False):
			with open(hostsFile, "w+") as outFile:
				outFile.write("#\tThis is the default file containing the list of hosts to connect to\n#This is the file that will be used if no '-h' argument is specified\n\n#\tExpected formatting as:\n#Host=<hostname>\tWhere <hostname> is the hostname alias as specified in your ~/.ssh/config file\n\n")

		#	Create the secret file if it doesn't exist
		secretFile = os.path.join(applicationFolder, "secret")
		if (os.path.exists(secretFile + ".crypt") == False) and (os.path.exists(secretFile) == False):
			with open(secretFile, "w+") as outFile:
				outFile.write("#\tPlease use the encryptSecretFile.py tool to encrypt this after entering the sudo passphrases for each host.\n\n#\tExpected formatting is:\n#<Host>=<passphrase>\n\n")

		#	Create the local downloads folder if it doens't exist
		if (os.path.exists(localDownloadFolder) == False):
			os.makedirs(localDownloadFolder)

		return

	#	If this fails, print out an error and exit
	except Exception as e:
		print(f"Error creating application folder. {e}.")
		sys.exit(1)


#	readHostFile	-	This function will read the supplied hosts file, listning the endpoints to connect to.  This will only return lines which start with proper keywords.
#
#	Inputs:		hostFile	-	The filename/filepath of the hosts file to read from.
#				logFilePath	-	The current log file to use for this application.
#
#	Outputs:	A List of strings, containing the desired hosts to connect to.
#
def readHostFile(hostFile, logFilePath):

	#	Define the list of raw hosts to return
	rawHosts = []

	#	Try to read the file
	try:
		#	Open the hosts file...
		with open(hostFile, "r") as inFile:
			#	Read the file line-by-line
			for line in inFile.readlines():
				#	Trim off right-hand whitespace and comments
				line = line.split("#")[0].strip()
				#	Split on spaces and equal signs
				line = line.replace("="," ")
				line = re.sub(' +',' ',line)
				#	If the line appears to be a valid hostfile line, append it to the list of rawHosts
				if (line.lower().startswith('host ')):
					#	Take the 2nd element, removing the leading "Host" keyword
					line = line.split(' ')[1].strip()
					rawHosts.append(line)

	#	Catch any exceptions and log them in the log file
	except Exception as e:
		log(logFilePath, logErrorCode, f"Error occured while reading supplied Hosts file: {e}. ", "Host Formatting-readHostFile")
		print(f"Failed to read given host file: {hostFile}.  {e}.")
		return []

	return rawHosts


#	Handle this script being called from the command-line
if (__name__ == "__main__"):

	#	If arguments are passed in...
	if (len(sys.argv) > 1):
		#	Handle the command-line arguments which were passed in
		formattedArguments = formatArguments(validArguments, sys.argv[1:], defaultArguments)
		
		#	Call the main function with those arguments
		if (formattedArguments):
			main(*formattedArguments)
	
	#	Otherwise, call main by default
	else:
		main()