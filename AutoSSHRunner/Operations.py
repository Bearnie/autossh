#!/usr/bin/env python3

#	Author:		Joseph Sadden
#	Date:		15th January, 2019
#	Revision:	2.0

#	Import Library Packages
import os
import sys

#   Append the root folder for the project to the import path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(sys.argv[0]), "..")))

#	Import Local Files
from Common.logFile import appendToLog as log, logErrorCode, logWarningCode, logStatusCode	#	Import the local logging module

#   main    -   This is the top-level function in charge of running the autoSSH process on the remote host.
#
#   Inputs:     connection      -   The currently open and active Fabric Connection object to the remote host.
#               host            -   The symbolic name of the current host.
#               remoteFolder    -   The remote folder to upload files and execute scripts from.
#               filesToSend     -   The list of files to just transfer to the remote host.
#               preCommands     -   The list of commands to run before any scripts on the remote host.
#               filesToExecute  -   The list of files to execute on the remote host.
#               postCommands    -   The list of commands to run after all the scripts have finished.
#               filesToRetrieve -   The list of files to retrieve after the last commands have finished.
#               localFolder     -   The local folder to download files to.
#               sudoFlag        -   A flag indicating whether or not commands and script should be run under "sudo"
#               logFilePath     -   The path to the log file to use.
#
#   Outputs:    None.
#
def main(connection, host, remoteFolder, filesToSend, preCommands, filesToExecute, postCommands, filesToRetrieve, localFolder, sudoFlag, logFilePath):

    remoteDirCreated = False
    #   Create the remote directory on the host if there are any scripts to run or upload
    if (filesToSend != []) or (filesToExecute != []):
        createRemoteDirectory(connection, remoteFolder, logFilePath)
        remoteDirCreated = True

    #   Send over all of the files requested (if any)
    for f in (filesToSend + filesToExecute):
        sendFile(connection, f, remoteFolder, logFilePath)

    #   Run each of the pre-commands (if any)
    for command in preCommands:
        runCommand(connection, command, sudoFlag, logFilePath)

    #   Run each of the script files (if any)
    for f in filesToExecute:
        runFile(connection, f, remoteFolder, sudoFlag, logFilePath)

    #   Run each of the post-commands (if any)
    for command in postCommands:
        runCommand(connection, command, sudoFlag, logFilePath)

    #   Retrieve each of the files specified (if any)
    for f in filesToRetrieve:
        getFile(connection, host, f, localFolder, logFilePath)

    #   Remove the temporary remote directory, if it exists and there were no files marked as send-only
    if (filesToSend == []):
        removeRemoteDirectory(connection, remoteFolder, remoteDirCreated, logFilePath)
    
    #   If there were files marked as send-only, remove any of the script-files transferred to the remote directory (if any)
    else:
        cleanRemoteDirectory(connection, remoteFolder, filesToExecute, logFilePath)

    return


#   createRemoteDirectory   -   This function will create the given remote directory, 
#
#   Inputs:     connection      -   The currently open and active Fabric Connection object to the remote host.
#               remoteFolder    -   The remote folder to upload files and execute scripts from.
#               logFilePath     -   The path to the log file to use.
#
#   Outputs:    None
#
def createRemoteDirectory(connection, remoteFolder, logFilePath):

    #   Check if the remote directory exists, and if not, create it.
    command=f"if [ ! -e {remoteFolder} ]; then mkdir {remoteFolder}; fi"

    #   Issue the command to the remote host.
    connection.run(command)

    print(f"Created temporary remote directory: {remoteFolder}")

    return


#   sendFile    -   Transfers a file to the remote host, using SFTP over the active connection.
#
#   Inputs:     connection      -   The currently open and active Fabric Connection object to the remote host.
#               f               -   The file to send over the connection to the remote host.
#               remoteFolder    -   The remote folder to upload files and execute scripts from.
#               logFilePath     -   The path to the log file to use.
#
#   Outputs:    None
#
def sendFile(connection, f, remoteFolder, logFilePath):

    #   Send the file over the SFTP connection.
    connection.put(f, f"{remoteFolder}/{os.path.basename(f)}")

    print(f"Sent file: {os.path.basename(f)}.")
    
    return


#   runCommand  -   Runs a shell command on the remote host, using the active connection.
#
#   Inputs:     connection      -   The currently open and active Fabric Connection object to the remote host.
#               command         -   The shell command to issue to the remote host.
#               sudoFlag        -   A flag indicating whether or not commands and script should be run under "sudo"
#               logFilePath     -   The path to the log file to use.
#
#   Outputs:    None
#
def runCommand(connection, command, sudoFlag, logFilePath):

    if (sudoFlag):
        connection.sudo(command)
    else:
        connection.run(command)
    
    return


#   runFile -   Executes a script file on the remote host, using the active connection.
#
#   Inputs:     connection      -   The currently open and active Fabric Connection object to the remote host.
#               f               -   The file to run on the remote host.
#               remoteFolder    -   The remote folder to upload files and execute scripts from.
#               sudoFlag        -   A flag indicating whether or not commands and script should be run under "sudo"
#               logFilePath     -   The path to the log file to use.
#
#   Outputs:    None
#
def runFile(connection, f, remoteFolder, sudoFlag, logFilePath):

    #   Set the permission of the file, and then run as root
    command = f"chmod +x {remoteFolder}/{os.path.basename(f)}; {remoteFolder}/./{os.path.basename(f)}"
    print(f"Running file: {os.path.basename(f)}.")
    runCommand(connection, command, sudoFlag, logFilePath)
    
    return


#   getFile -   Retrieves a file from the remote host, using the active connection.
#
#   Inputs:     connection      -   The currently open and active Fabric Connection object to the remote host.
#               host            -   The symbolic name of the current host.
#               f               -   The file to retrieve from the remote host.
#               localFolder     -   The local folder to download files from the remote host into.
#               logFilePath     -   The path to the log file to use.
#
#   Outputs:    None
#
def getFile(connection, host, f, localFolder, logFilePath):

    #   Retrieve the file over an SFTP connection
    connection.get(f, f"{localFolder}/{host}_{os.path.basename(f)}")

    print(f"Retrieved file: {os.path.basename(f)}.")
    
    return


#   removeRemoteDirectory   -   Deletes the temporary working folder from the remote host.
#
#   Inputs:     connection      -   The currently open and active Fabric Connection object to the remote host.
#               remoteFolder    -   The remote folder to upload files and execute scripts from.
#               remoteDirCreated-   A flag indicating whether or not the remote directory was actually created.
#               logFilePath     -   The path to the log file to use.
#
#   Outputs:    None
#
def removeRemoteDirectory(connection, remoteFolder, remoteDirCreated, logFilePath):

    #   If the remote folder exists, remove it.
    if (remoteDirCreated == True):
        command = f"if [ -e {remoteFolder} ]; then rm -rf {remoteFolder}; fi"
        connection.run(command)
        print(f"Removed temporary remote directory: {remoteFolder}.")
    
    return


#   cleanRemoteDirectory   -   Deletes any script files from the temporary working folder from the remote host.
#
#   Inputs:     connection      -   The currently open and active Fabric Connection object to the remote host.
#               remoteFolder    -   The remote folder to upload files and execute scripts from.
#               filesToRemove   -   The list of files to remove from the temp directory.
#               logFilePath     -   The path to the log file to use.
#
#   Outputs:    None
#
def cleanRemoteDirectory(connection, remoteFolder, filesToRemove, logFilePath):

    #   Remove each file one by one
    for f in filesToRemove:
        command = f"rm {remoteFolder}/{os.path.basename(f)}"
        connection.run(command)

        print(f"Removed file: {os.path.basename(f)}.")

    return