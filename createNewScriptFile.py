#!/usr/bin/env python3

#   Author:     Joseph Sadden
#   Date:       7th December, 2018
#   Revision:   1.0

#   Import Library Packages
import sys


#       SCRIPT PURPOSE
#
#       This script is used to generate a standard template for new Python Files for a project.


#   main    -   This function creates a new file with either the specified name, or a default name.  It then prints out the default skeleton for the file, including the heading and main definition.
#
#   Inputs  -   (optional) Command-line argument indicating the filename to use
#   Outputs -   None, the file is created in the same directory as this script is called from
#
def main(filename='newFile.py', fileType='-m'):
    
    #   If a main script is specified, make a python template
    if (fileType == '-m'):
        makeMain(filename)
    #   If a secondary script is specified, make a python template
    elif (fileType == '-s'):
        makeSecondary(filename)

    return

def makeMain(filename):

    makeSecondary(filename)

    with open(filename, "a+") as outFile:
        outFile.write('if (__name__ == "__main__"):\n\tmain()')

    return

def makeSecondary(filename):

    #   Open the file, creating it...
    with open(filename, 'w+') as outFile:
        #   Write out the skeleton of the program to use
        outFile.write('' + 
                '#!/usr/bin/env python3\n\n' + 
                '#\tAuthor:\t\tJoseph Sadden\n' +
                '#\tDate:\t\t? ?, ?\n' +
                '#\tRevision:\t1.0\n\n' +
                '#\tImport Library Packages\nimport os\nimport sys\n\n' +
                '#\tAppend the root folder for the project to the import path\nsys.path.append(os.path.abspath(os.path.join(os.path.dirname(sys.argv[0]), "..")))\n\n'
                '#\tImport Local Files\n\n\n' +
                '#\tScript-wide Definitions\n\n\n' +
                '#\t\tSCRIPT PURPOSE\n' +
                '#\n' +
                '#\t\tThis script\n' +
                '\n\n' +
                '#\n#\n#\n#\n#\ndef main():\n' +
                '\treturn\n\n\n')

    return

def printHelp():

    print('\n./createNewScriptFile.py <filename> <fileType [-m -s]>\n')

    exit(0)

#   Allow this script to be called from the command-line
if (__name__ == '__main__'):

    if (len(sys.argv) == 1):
        main()
    elif (len(sys.argv) == 2):
        if (sys.argv[1] == '--help'):
            printHelp()
        main(filename=sys.argv[1])
    elif (len(sys.argv) == 3):
        main(filename=sys.argv[1], fileType=sys.argv[2])
